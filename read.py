import Image

def get_pixel(filename): 

    print "Loading..."
    img       = Image.open(filename)
    pixels    = img.load()
    width     = img.size[0]
    height    = img.size[1]
    print "%dx%d pixels" % img.size 


    # Calculate gamma correction table.  This includes
    # LPD8806-specific conversion (7-bit color w/high bit set).
    gamma = bytearray(256)
    for i in range(256):
            gamma[i] = 0x80 | int(pow(float(i) / 255.0, 2.5) * 127.0 + 0.5)
             

    # Create list of bytearrays, one for each column of image.
    # R, G, B byte per pixel, plus extra '0' byte at end for latch.
    print "Allocating..."


    foo = []
    column = [[foo for i in range(width)] for j in range(height)]
    # Convert 8-bit RGB image into column-wise GRB bytearray list.
    print "Converting..."
    for x in range(width):
        for y in range(height):
            value = list(pixels[x, y]) 
            print(value)
            
            column[x][y] = [gamma[value[0]], gamma[value[1]], gamma[value[2]]]
     
     
    # Then it's a trivial matter of writing each column to the SPI port.
    print "Displaying..." 
    return column

print get_pixel('fon04.png')

