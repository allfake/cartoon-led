import math
import time
from LedStrip_WS2801 import LedStrip_WS2801
import Image
import PIL.ImageOps
import numpy as np

def invert(image):
    "Invert a channel"

    image.load()
    return image._new(image.im.chop_invert())

def get_pixel(filename, fn, dx, dy): 
    img       = Image.open(filename)
    pixels    = img.load()
    width     = img.size[0]
    height    = img.size[1]

    # Calculate gamma correction table.  This includes
    # LPD8806-specific conversion (7-bit color w/high bit set).
    gamma = bytearray(256)
    for i in range(256):
            gamma[i] = 0x80 | int(pow(float(i) / 255.0, 2.5) * 127.0 + 0.5)
             

    # Create list of bytearrays, one for each column of image.
    # R, G, B byte per pixel, plus extra '0' byte at end for latch.

    foo = []
    column = [[foo for i in range(width)] for j in range(height)]
    # Convert 8-bit RGB image into column-wise GRB bytearray list.
 
    for x in range(width):
        for y in range(height):
            value = list(pixels[x, y]) 
            column[y][x] = [value[0], value[2], value[1]] 
            #column[y][x] = [gamma[value[0]], gamma[value[2]], gamma[value[1]]] 
    column = move_right(column, dx, dy) 
      
    # Then it's a trivial matter of writing each column to the SPI port.
    return column

def move_right(img, dx, dy): 
  width     = 7#img.size[0]
  height    = 7#img.size[1]

  move =  [[1, 0, dx], [0, 1, dy], [0, 0, 1]] 
  img_p = [[[0 for k in xrange(3)] for j in xrange(7)] for i in xrange(7)]    
  for x in range(width):
    for y in range(height):
      try:
        pos = [[x], [y], [1]]
        pos_p = np.dot(move, pos)
        img_p[pos_p[1][0]][pos_p[0][0]] = img[y][x]
      except:
        pass
  return img_p

r = [255, 0, 0]
g = [0, 255, 0]
b = [0, 0, 255]
x = [0, 0, 0]
y = [255, 0, 255]
br = [139, 19, 69]
a = [
 [x, y, y, y, y, y, x],

 [y, br, br, x, br, br, y],

 [y, x, r, x, r, x, y],

 [y, x, x, x, x, x, y],

 [y, x, b, b, b, x, y],

 [y, x, x, x, x, x, y],

 [x, y, y, y, y, y, x],
]


def fillArray(ledStrip, a, sleep):
  result = []
  for idx, i in enumerate(a):
    for idy, j in enumerate(i):
      if idx % 2:
        result.append(a[idx][len(a) - idy - 1])
      else:
        result.append(a[idx][idy])
  for i in range(0, ledStrip.nLeds):
    if i < 49:
      ledStrip.setPixel(i, result[i])
  ledStrip.update()
ledStrip = LedStrip_WS2801("/dev/spidev0.0", 50)
#fillArray(ledStrip, get_pixel('tuck-01.png'), 0.05)
#fillArray(ledStrip, get_pixel('fon01.png'), 0.05)
ct = 0

while True:
#  for i in range(5):
#    fillArray(ledStrip, get_pixel('fon01.png'), 0.0)
#    time.sleep(0.5)
#    fillArray(ledStrip, get_pixel('fon02.png'), 0.0)
#    time.sleep(0.5)
#    fillArray(ledStrip, get_pixel('fon03.png'), 0.0)
#    time.sleep(0.5)
#    fillArray(ledStrip, get_pixel('fon04.png'), 0.0)
#    time.sleep(0.5)
#  for i in range(8):
#    fillArray(ledStrip, get_pixel('tuck-01.png'), 0.0)
#    time.sleep(0.5)
#    fillArray(ledStrip, get_pixel('tuck-02.png'), 0.0)
#    time.sleep(0.5)
#    fillArray(ledStrip, get_pixel('tuck-03.png'), 0.0)
#    time.sleep(0.5)
#    fillArray(ledStrip, get_pixel('tuck-04.png'), 0.0)
#    time.sleep(0.5)
#  for i in range(12):
#    fillArray(ledStrip, get_pixel('nemo1.png'), 0.0)
#    time.sleep(0.5)
#    fillArray(ledStrip, get_pixel('nemo2.png'), 0.0)
#    time.sleep(0.5)
  before_step = 0
  step = 0
  for i in range(13):
    if before_step <= 5:
      fillArray(ledStrip, get_pixel('dog1.png', 'move_right', 0, 0), 0.0)
      time.sleep(0.2) 
      fillArray(ledStrip, get_pixel('dog2.png', 'moev_right', 0, 0), 0.0)
      time.sleep(0.2)
      fillArray(ledStrip, get_pixel('dog3.png', 'move_right', 0, 0), 0.0)
      time.sleep(0.2)
      before_step += 1
    else: 
      fillArray(ledStrip, get_pixel('dog0.png', 'move_right', -step, 1), 0.0)
      time.sleep(0.08) 
      step += 1


